---
postfix_mydestinations:
  - "$myhostname"
  - "localhost"
postfix_myhostname: "{{ inventory_hostname }}"

postfix_mailname_path: "/etc/mailname"
postfix_aliases_path: "/etc/aliases"
postfix_virtual_path: "/etc/postfix/virtual"
postfix_sender_access_path: "/etc/postfix/sender_access"

postfix_spamhaus_restrictions: 
  - "reject_rbl_client {{ postfix_spamhaus_DQS_key }}.zen.dq.spamhaus.net=127.0.0.[2..11]"
  - "reject_rhsbl_sender {{ postfix_spamhaus_DQS_key }}.dbl.dq.spamhaus.net=127.0.1.[2..99]"
  - "reject_rhsbl_helo {{ postfix_spamhaus_DQS_key }}.dbl.dq.spamhaus.net=127.0.1.[2..99]"
  - "reject_rhsbl_reverse_client {{ postfix_spamhaus_DQS_key }}.dbl.dq.spamhaus.net=127.0.1.[2..99]"
  - "reject_rhsbl_sender {{ postfix_spamhaus_DQS_key }}.zrd.dq.spamhaus.net=127.0.2.[2..24]"
  - "reject_rhsbl_helo {{ postfix_spamhaus_DQS_key }}.zrd.dq.spamhaus.net=127.0.2.[2..24]"
  - "reject_rhsbl_reverse_client {{ postfix_spamhaus_DQS_key }}.zrd.dq.spamhaus.net=127.0.2.[2..24]"

postfix_spamhaus_dnsbl_reply_map: |
  {{ postfix_spamhaus_DQS_key }}.zen.dq.spamhaus.net=127.0.0.[2..11]  554 $rbl_class $rbl_what blocked using ZEN - see https://www.spamhaus.org/query/ip/$client_address for details
  {{ postfix_spamhaus_DQS_key }}.dbl.dq.spamhaus.net=127.0.1.[2..99]  554 $rbl_class $rbl_what blocked using DBL - see $rbl_txt for details
  {{ postfix_spamhaus_DQS_key }}.zrd.dq.spamhaus.net=127.0.2.[2..24]  554 $rbl_class $rbl_what blocked using ZRD - domain too young
  {{ postfix_spamhaus_DQS_key }}.zen.dq.spamhaus.net      554 $rbl_class $rbl_what blocked using ZEN - see https://www.spamhaus.org/query/ip/$client_address for details
  {{ postfix_spamhaus_DQS_key }}.dbl.dq.spamhaus.net      554 $rbl_class $rbl_what blocked using DBL - see $rbl_txt for details
  {{ postfix_spamhaus_DQS_key }}.zrd.dq.spamhaus.net      554 $rbl_class $rbl_what blocked using ZRD - domain too young

postfix_my_default_networks:
  - "127.0.0.0/8"
  - "[::ffff:127.0.0.0]/104"
  - "[::1]/128"

postfix_settings:
  alias_database: "hash:{{ postfix_aliases_path }}"
  alias_maps: "hash:{{ postfix_aliases_path }}"
  append_dot_mydomain: "no"
  biff: "no"
  compatibility_level: 2
  inet_interfaces: "all"
  inet_protocols: "all"
  mailbox_size_limit: 0
  mydestination: "{{ postfix_mydestinations | join(', ') }}"
  myhostname: "{{ postfix_myhostname }}"
  mynetworks: "{{ (postfix_my_default_networks + postfix_my_custom_networks) | join(' ') }}"
  myorigin: "{{ postfix_myhostname }}"
  rbl_reply_maps: "{{ '' if (postfix_spamhaus_DQS_key | length == 0) else 'hash:$config_directory/dnsbl-reply-map' }}"
  readme_directory: "no"
  recipient_delimiter: "+"
  relayhost: ""
  smtp_destination_concurrency_limit: 2
  smtp_destination_rate_delay: "1s"
  smtp_extra_recipient_limit: "10"
  smtp_tls_session_cache_database: "btree:${data_directory}/smtp_scache"
  smtpd_banner: "$myhostname ESMTP $mail_name"
  smtpd_recipient_restrictions: "permit_mynetworks {% if postfix_spamhaus_DQS_key | length > 0 %}{{ postfix_spamhaus_restrictions | join(' ') }} {% endif %}permit_sasl_authenticated reject_unauth_destination check_sender_access hash:{{ postfix_sender_access_path }}"
  smtpd_relay_restrictions: "permit_mynetworks permit_sasl_authenticated defer_unauth_destination"
  smtpd_sender_restrictions: "permit_mynetworks reject_unknown_sender_domain warn_if_reject reject_unverified_sender"
  smtpd_sasl_auth_enable: "yes"
  smtpd_sasl_local_domain: "$myhostname"
  smtpd_sasl_type: "dovecot"
  smtpd_sasl_path: "private/auth"
  smtpd_sasl_security_options: "noanonymous"
  smtpd_tls_cert_file: "{{ postfix_smtpd_tls_cert_file }}"
  smtpd_tls_key_file: "{{ postfix_smtpd_tls_key_file }}"
  smtpd_tls_session_cache_database: "btree:${data_directory}/smtpd_scache"
  smtpd_use_tls: "yes"
  virtual_alias_domains: "{{ postfix_virtual_alias_domains | join(' ') }}"
  virtual_alias_maps: "hash:{{ postfix_virtual_path }}"
